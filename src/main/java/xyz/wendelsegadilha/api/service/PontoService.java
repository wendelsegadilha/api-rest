package xyz.wendelsegadilha.api.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Service;

import xyz.wendelsegadilha.api.model.Ponto;
import xyz.wendelsegadilha.api.repository.PontoRepository;
import xyz.wendelsegadilha.api.service.exceptions.DataInvalidaException;
import xyz.wendelsegadilha.api.service.exceptions.IdNotFoundException;

@Service
public class PontoService {

	PontoRepository pontoDAO = new PontoRepository();

	public Ponto buscar(String id, String data) {
		
		if(isId(id)) {
			data = formataData(data);
			return pontoDAO.carregarDados(id, data);
		}
		
		throw new IdNotFoundException("ID não encontrado");

	}
	
	private boolean isId(String id){
		
		List<String> ids = new ArrayList<>();
		ids.addAll(Arrays.asList("id0001_temp", "id0002_temp", "id0003_temp"));
		return ids.contains(id);
		
	}
	
	public String formataData(String data) {
		
		SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
		Date dt = null;
		
		try {
			dt = format.parse(data);
		} catch (ParseException e) {
			throw new DataInvalidaException("Data inválida");
		}

		format = new SimpleDateFormat("yyyy-MM-dd");
		String dataFormatada = format.format(dt);
		
		return dataFormatada;
	}

}
