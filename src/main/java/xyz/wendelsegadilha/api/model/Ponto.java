package xyz.wendelsegadilha.api.model;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Ponto {
	
	@ApiModelProperty(value = "Código do ponto de monitoramento")
	private String id;
	@ApiModelProperty(value = "Nome do ponto de monitoramento")
	private String nome;
	
	@ApiModelProperty(value = "Períodos de monitoramento")
	@DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private List<LocalDateTime> timestamp = new ArrayList<>();
	
	@ApiModelProperty(value = "Lista de temperaturas")
	private List<Double> temperatura = new ArrayList<>();

}
