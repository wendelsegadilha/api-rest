package xyz.wendelsegadilha.api.repository;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.springframework.stereotype.Component;

import xyz.wendelsegadilha.api.model.Ponto;

@Component
public class PontoRepository {
	
	private final String ID0001_TEMP_ARQ = "id0001.csv";
	private final String ID0002_TEMP_ARQ = "id0002.csv";
	private final String ID0003_TEMP_ARQ ="id0003.csv";
	private final String[] NOME = {"[Ambiente] Armazém 03", "[Umidade 01] Sala Heparina", "[Refrigerador] Divisão"};
	private final String[] IDS = {"ID0001_TEMP", "ID0002_TEMP", "ID0003_TEMP"}; 

	public Ponto carregarDados(String id, String data) {
		
		Ponto ponto = new Ponto();
		ponto = getPonto(id);
		
		String path = getCaminhoArquivo(id);

		try (BufferedReader br = new BufferedReader(new FileReader(path))) {
			
			String line = br.readLine();
			line = br.readLine();
			while (line != null) {

				String[] vect = line.split(",");
				if (vect[0].contains(data)) {
					ponto.getTimestamp().add(LocalDateTime.parse(vect[0], DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
					ponto.getTemperatura().add(Double.parseDouble(vect[1]));
				}

				line = br.readLine();
			}

		} catch (IOException e) {
			System.out.println("Error: " + e.getMessage());
		}

		return ponto;
	}
	
	
	private Ponto getPonto(String id) {
		
		Ponto ponto = new Ponto();
		
		if (id.equalsIgnoreCase(IDS[0])) {
			ponto.setId(id);
			ponto.setNome(NOME[0]);
			
		} else if (id.equalsIgnoreCase(IDS[1])) {
			ponto.setId(id);
			ponto.setNome(NOME[1]);
			
		} else if (id.equalsIgnoreCase(IDS[2])) {
			ponto.setId(id);
			ponto.setNome(NOME[2]);
		}
		
		return ponto;

	}

	private String getCaminhoArquivo(String id) {

		if (id.equalsIgnoreCase(IDS[0])) {
			return ID0001_TEMP_ARQ;
		} else if (id.equalsIgnoreCase(IDS[1])) {
			return ID0002_TEMP_ARQ;
		} else if (id.equalsIgnoreCase(IDS[2])) {
			return ID0003_TEMP_ARQ;
		}
		
		return null;
	}

}
