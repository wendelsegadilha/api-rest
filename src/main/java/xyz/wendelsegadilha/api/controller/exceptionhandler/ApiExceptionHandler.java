package xyz.wendelsegadilha.api.controller.exceptionhandler;

import java.time.LocalDateTime;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import xyz.wendelsegadilha.api.service.exceptions.DataInvalidaException;
import xyz.wendelsegadilha.api.service.exceptions.IdNotFoundException;

@ControllerAdvice
public class ApiExceptionHandler {
	
	
	@ExceptionHandler(IdNotFoundException.class)
    public ResponseEntity<StandardError> objectNotFound(IdNotFoundException ex, HttpServletRequest request) {
        StandardError error = new StandardError(LocalDateTime.now(), HttpStatus.NOT_FOUND.value(),
                ex.getMessage(), request.getRequestURI());
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(error);
    }
	
	@ExceptionHandler(DataInvalidaException.class)
    public ResponseEntity<StandardError> dataInvalidaException(DataInvalidaException ex, HttpServletRequest request) {
        StandardError error = new StandardError(LocalDateTime.now(), HttpStatus.BAD_REQUEST.value(),
                ex.getMessage(), request.getRequestURI());
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(error);
    }

}
