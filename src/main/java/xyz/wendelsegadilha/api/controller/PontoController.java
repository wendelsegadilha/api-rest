package xyz.wendelsegadilha.api.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import xyz.wendelsegadilha.api.model.Ponto;
import xyz.wendelsegadilha.api.service.PontoService;

@RestController
@RequestMapping(value = "/")
public class PontoController {

	@Autowired
	private PontoService pontoService;
	
	@ApiOperation(value = "Retorna um ponto de monitoramento com os seus dados coletados de acordo com o id do ponto e uma data especificada")
	@ApiResponses(value = {
	    @ApiResponse(code = 500, message = "Foi gerada uma exceção"),
	})
	@GetMapping(value = "/ponto/{id}/{data}")
	public ResponseEntity<Ponto> buscar(@PathVariable String id, @PathVariable String data){
		
		Ponto ponto = pontoService.buscar(id, data);
		return ResponseEntity.ok().body(ponto);
		
	}
	
}
