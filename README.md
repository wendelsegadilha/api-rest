# Teste para Vaga de Desenvolvedor JAVA

### Descrição
Esse é um projeto de API REST que foi desenvolvido utlilizando as mais populares e consolidadas tecnologias do mercado de desenvolvimento de softwares atual. O projeto conta com a funcionalidade de consulta de pontos de monitoramentos de dados como temperatura, umidade, e etc...

### Funcionalidades
- Consulta de pontos de monitoramentos

### Tecnologias utilizadas: 
- Java
- Spring Boot

### Como utilizar esse projeto?
Primeiro, na classe PontoRepository.java é necessário alterar a localização dos arquivos .csv contendo os dados de cada ponto de monitoramento:
```java
// Especificar a localização dos arquivos
private final String ID0001_TEMP_ARQ = "id0001.csv";
private final String ID0002_TEMP_ARQ = "id0002.csv";
private final String ID0003_TEMP_ARQ ="id0003.csv";
```

Segundo, agora é só navegar até o diretório do projeto via terminal e executar:
```java
mvn clean package
```
para gerar o executável da aplicação.

Terceiro, abra seu navegado e acesse http://localhost:8080/ponto/id0001_temp/01-01-2022

Faça alguns testes, modifique o id do ponto. Você pode usar qualquer um desses:
```java
id0001_temp
id0002_temp
id0003_temp
```
Teste outras datas, fique ligado no padrão, dia-mês-ano.

### Confira a documentação
Abra seu navegado e acesse http://localhost:8080/swagger-ui.html


### E aí dev, vamos bater um papo?
- [LinkedIn](https://www.linkedin.com/in/wendel-segadilha-490b14199/)
- [YouTube](https://www.youtube.com/wendelsegadilha)
